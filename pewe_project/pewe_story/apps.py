from django.apps import AppConfig


class PeweStoryConfig(AppConfig):
    name = 'pewe_story'
